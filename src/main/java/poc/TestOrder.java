/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package poc; //proof of concept

import com.werapan.databaseproject.dao.OrderDao;
import com.werapan.databaseproject.model.Order;
import com.werapan.databaseproject.model.OrderDetail;
import com.werapan.databaseproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author acer
 */
public class TestOrder {

    public static void main(String[] args) {
        Product product1 = new Product(1, "A", 75);
        Product product2 = new Product(2, "B", 180);
        Product product3 = new Product(3, "C", 70);
        Order order = new Order();
//        OrderDetail orderDetail1 = new OrderDetail(product1, product1.getName(), product1.getPrice(), 1, order);
//        ArrayList<OrderDetail> orderDetails = order.getOrderDetail();
//        orderDetails.add(orderDetail1);
        order.addOrderDetail(product1, 1);
        order.addOrderDetail(product2, 1);
        order.addOrderDetail(product3, 1);

//        System.out.println(order);
//        System.out.println(order.getOrderDetail());
        printReciept(order);

        OrderDao orderDao = new OrderDao();
//        Order newOrder = orderDao.save(order);
//        System.out.println(newOrder);
        System.out.println(orderDao.get(3));
    }

    static void printReciept(Order order) {
        System.out.println("Order " + order.getId());
        for (OrderDetail ord : order.getOrderDetail()) {
            System.out.println(" " + ord.getProductName() + " " + ord.getQty() + " " + ord.getProductPrice() + " " + ord.getTotal());
        }
        System.out.println("Total: " + order.getTotal() + " Qty: " + order.getQty());
    }
}
